import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Product } from '../product.interface';
import { ProductService } from '../product.service';
import { Observable, EMPTY } from 'rxjs';
import { catchError, count, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent implements OnInit {

  title = 'Products';
  products: Array<Product>;
  products$: Observable<Product[]>;
  selectedProduct: Product;
  errorMessage: string;
  productNumber = 0;

  sorter = '-price';

  // pagination
  pageSize = 9;
  start = 0;
  end = this.pageSize;
  currentPage = 1;

  constructor(private productService: ProductService, private router: Router) {
  }

  onSelect(product: Product) {
    this.router.navigateByUrl(`/products/${product.id}`);
  }

  ngOnInit() {
    this.products$ = this.productService.products$.pipe(
      tap(products => this.productNumber = products.length),
      catchError(error => { this.errorMessage = error; return EMPTY; }));
    // this.productService
    //   .products$
    //   .subscribe(products => {
    //     this.products = products;
    //   });
  }

  sortList(propertyName: string) {
    this.sorter = this.sorter.startsWith('-') ? propertyName : `-${propertyName}`;
  }

  nextPage() {
    this.start += this.pageSize;
    this.end += this.pageSize;
    this.selectedProduct = null;
    this.currentPage++;
  }
  previousPage() {
    this.start -= this.pageSize;
    this.end -= this.pageSize;
    this.selectedProduct = null;
    this.currentPage--;
  }

}
