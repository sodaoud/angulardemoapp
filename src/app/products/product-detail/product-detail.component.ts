import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../product.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../product.service';
import { flatMap, filter } from 'rxjs/operators';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css'],
})
export class ProductDetailComponent implements OnInit {

  product: Product;

  constructor(private activatedRoute: ActivatedRoute, private productService: ProductService, private router: Router) { }
  delete(id: number) {
    if (window.confirm('SURE ?')) {
      this.productService.deleteProduct(id).subscribe(response => this.router.navigateByUrl('/products'));
    }
  }
  ngOnInit() {
    const id = this.activatedRoute.snapshot.params.id;
    this.productService.products$
      .pipe(
        flatMap(products => products),
        filter(product => product.id === parseInt(id))
      )
      .subscribe(product => this.product = product);
  }

}
