import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { OrderBy } from './orderBy.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductInsertComponent } from './product-insert/product-insert.component';


@NgModule({
  declarations: [
    ProductListComponent,
    ProductDetailComponent,
    ProductInsertComponent,
    OrderBy,
  ],
  imports: [
    FormsModule,
    CommonModule,
    ProductsRoutingModule,
    ReactiveFormsModule,
  ],
  exports: [
    ProductListComponent,
  ]
})
export class ProductsModule { }
