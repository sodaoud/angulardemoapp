import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Product } from './product.interface';
import { Observable, throwError } from 'rxjs';
import { catchError, delay, tap, filter, mergeAll, shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private baseUrl = 'https://storerestservice.azurewebsites.net/api/products/';

  constructor(private http: HttpClient) {
    this.initProducts();
  }

  products$: Observable<Product[]>;

  createProduct(product: Product) {
    return this.http.post(this.baseUrl, product).pipe(tap(() => this.initProducts()));
  }

  deleteProduct(id: number): Observable<any> {
    return this.http.delete(this.baseUrl + id).pipe(tap(() => this.initProducts()));
  }

  initProducts() {
    this.products$ = this.http.get<Array<Product>>(this.baseUrl)
      .pipe(
        shareReplay(),
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    // in a real world app, you may send the error to the server using some remote logging infrastructure
    // instead of just logging it to the console
    let errorMsg: string;
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMsg = 'An error occurred:' + error.error.message;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      errorMsg = `Backend returned code ${error.status}, body was: ${error.error}`;
    }
    console.error(errorMsg);
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}
